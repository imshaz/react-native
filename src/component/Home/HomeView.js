import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import HomeViewStyles from './HomeViewStyle';

export default function HomeView() {
  return (
    <View style={styles.wrap}>
      <View style={HomeViewStyles.welcomeHeader}>
        <Text>Good Morning</Text>
      </View>
      <View style={HomeViewStyles.welcomeButton}>
        <Text>Button</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
});

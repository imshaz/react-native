import {StyleSheet} from 'react-native';

const HomeViewStyles = StyleSheet.create({
  wrap: {
    flex: 1,
    flexDirection: 'column',
    // justifyContent: 'space-between',
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 30,
    backgroundColor: 'red',
  },
  welcomeHeader: {
    flex: 1,
    textAlign: 'center',
    fontSize: 36,
    color: '#000',
    backgroundColor: 'blue',
    height: 100,
  },
  welcomeButton: {
    flex: 2,
    height: 100,
    backgroundColor: 'orange',
    textAlign: 'center',
  },
});

export default HomeViewStyles;

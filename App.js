
import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
} from 'react-native';

import HomeView from './src/component/Home/HomeView'

const App: () => React$Node = () => {
  return (
    <SafeAreaView>
      <HomeView/> 
    </SafeAreaView>
  );
};


export default App;
